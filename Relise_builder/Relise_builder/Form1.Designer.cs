﻿
namespace Relise_builder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TP_VS = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.TP_VS_TB_VIDEO_IN = new System.Windows.Forms.TextBox();
            this.TP_VS_TB_SOUND = new System.Windows.Forms.TextBox();
            this.TP_VS_TB_VIDEO_OUT = new System.Windows.Forms.TextBox();
            this.TP_VS_B_RUN = new System.Windows.Forms.Button();
            this.TP_VS_B_SELECT_VIDEO_IN = new System.Windows.Forms.Button();
            this.TP_VS_B_SELECT_SOUND = new System.Windows.Forms.Button();
            this.TP_VS_B_SELECT_VIDEO_OUT = new System.Windows.Forms.Button();
            this.TP_VS_L_VIDEO = new System.Windows.Forms.Label();
            this.TP_VS_L_SOUND = new System.Windows.Forms.Label();
            this.TP_VS_L_VIDEO_OUT = new System.Windows.Forms.Label();
            this.B_SITE = new System.Windows.Forms.Button();
            this.B_SITE_UA = new System.Windows.Forms.Button();
            this.B_TG_BLOG = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.TP_VS.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.TP_VS);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 88);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(505, 350);
            this.tabControl1.TabIndex = 0;
            // 
            // TP_VS
            // 
            this.TP_VS.Controls.Add(this.TP_VS_L_VIDEO_OUT);
            this.TP_VS.Controls.Add(this.TP_VS_L_SOUND);
            this.TP_VS.Controls.Add(this.TP_VS_L_VIDEO);
            this.TP_VS.Controls.Add(this.TP_VS_B_SELECT_VIDEO_OUT);
            this.TP_VS.Controls.Add(this.TP_VS_B_SELECT_SOUND);
            this.TP_VS.Controls.Add(this.TP_VS_B_SELECT_VIDEO_IN);
            this.TP_VS.Controls.Add(this.TP_VS_B_RUN);
            this.TP_VS.Controls.Add(this.TP_VS_TB_VIDEO_OUT);
            this.TP_VS.Controls.Add(this.TP_VS_TB_SOUND);
            this.TP_VS.Controls.Add(this.TP_VS_TB_VIDEO_IN);
            this.TP_VS.Location = new System.Drawing.Point(4, 22);
            this.TP_VS.Name = "TP_VS";
            this.TP_VS.Padding = new System.Windows.Forms.Padding(3);
            this.TP_VS.Size = new System.Drawing.Size(497, 324);
            this.TP_VS.TabIndex = 0;
            this.TP_VS.Text = "Video + Sound";
            this.TP_VS.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.B_TG_BLOG);
            this.tabPage2.Controls.Add(this.B_SITE_UA);
            this.tabPage2.Controls.Add(this.B_SITE);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(497, 324);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "About";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Minecraft 1.1", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(10, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(418, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "VOLCUA Release Builder";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // TP_VS_TB_VIDEO_IN
            // 
            this.TP_VS_TB_VIDEO_IN.AllowDrop = true;
            this.TP_VS_TB_VIDEO_IN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_TB_VIDEO_IN.Location = new System.Drawing.Point(6, 55);
            this.TP_VS_TB_VIDEO_IN.Name = "TP_VS_TB_VIDEO_IN";
            this.TP_VS_TB_VIDEO_IN.ReadOnly = true;
            this.TP_VS_TB_VIDEO_IN.Size = new System.Drawing.Size(405, 20);
            this.TP_VS_TB_VIDEO_IN.TabIndex = 0;
            this.TP_VS_TB_VIDEO_IN.DragDrop += new System.Windows.Forms.DragEventHandler(this.TP_VS_TB_VIDEO_IN_DragDrop);
            this.TP_VS_TB_VIDEO_IN.DragEnter += new System.Windows.Forms.DragEventHandler(this.TP_VS_TB_VIDEO_IN_DragEnter);
            // 
            // TP_VS_TB_SOUND
            // 
            this.TP_VS_TB_SOUND.AllowDrop = true;
            this.TP_VS_TB_SOUND.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_TB_SOUND.Location = new System.Drawing.Point(6, 113);
            this.TP_VS_TB_SOUND.Name = "TP_VS_TB_SOUND";
            this.TP_VS_TB_SOUND.ReadOnly = true;
            this.TP_VS_TB_SOUND.Size = new System.Drawing.Size(405, 20);
            this.TP_VS_TB_SOUND.TabIndex = 1;
            this.TP_VS_TB_SOUND.DragDrop += new System.Windows.Forms.DragEventHandler(this.TP_VS_TB_SOUND_DragDrop);
            this.TP_VS_TB_SOUND.DragEnter += new System.Windows.Forms.DragEventHandler(this.TP_VS_TB_SOUND_DragEnter);
            // 
            // TP_VS_TB_VIDEO_OUT
            // 
            this.TP_VS_TB_VIDEO_OUT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_TB_VIDEO_OUT.Location = new System.Drawing.Point(6, 178);
            this.TP_VS_TB_VIDEO_OUT.Name = "TP_VS_TB_VIDEO_OUT";
            this.TP_VS_TB_VIDEO_OUT.ReadOnly = true;
            this.TP_VS_TB_VIDEO_OUT.Size = new System.Drawing.Size(405, 20);
            this.TP_VS_TB_VIDEO_OUT.TabIndex = 2;
            // 
            // TP_VS_B_RUN
            // 
            this.TP_VS_B_RUN.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_B_RUN.Location = new System.Drawing.Point(6, 241);
            this.TP_VS_B_RUN.Name = "TP_VS_B_RUN";
            this.TP_VS_B_RUN.Size = new System.Drawing.Size(485, 77);
            this.TP_VS_B_RUN.TabIndex = 3;
            this.TP_VS_B_RUN.Text = "RUN";
            this.TP_VS_B_RUN.UseVisualStyleBackColor = true;
            this.TP_VS_B_RUN.Click += new System.EventHandler(this.TP_VS_B_RUN_Click);
            // 
            // TP_VS_B_SELECT_VIDEO_IN
            // 
            this.TP_VS_B_SELECT_VIDEO_IN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_B_SELECT_VIDEO_IN.Location = new System.Drawing.Point(415, 53);
            this.TP_VS_B_SELECT_VIDEO_IN.Name = "TP_VS_B_SELECT_VIDEO_IN";
            this.TP_VS_B_SELECT_VIDEO_IN.Size = new System.Drawing.Size(75, 23);
            this.TP_VS_B_SELECT_VIDEO_IN.TabIndex = 4;
            this.TP_VS_B_SELECT_VIDEO_IN.Text = "Select";
            this.TP_VS_B_SELECT_VIDEO_IN.UseVisualStyleBackColor = true;
            this.TP_VS_B_SELECT_VIDEO_IN.Click += new System.EventHandler(this.TP_VS_B_SELECT_VIDEO_IN_Click);
            // 
            // TP_VS_B_SELECT_SOUND
            // 
            this.TP_VS_B_SELECT_SOUND.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_B_SELECT_SOUND.Location = new System.Drawing.Point(416, 113);
            this.TP_VS_B_SELECT_SOUND.Name = "TP_VS_B_SELECT_SOUND";
            this.TP_VS_B_SELECT_SOUND.Size = new System.Drawing.Size(75, 23);
            this.TP_VS_B_SELECT_SOUND.TabIndex = 5;
            this.TP_VS_B_SELECT_SOUND.Text = "Select";
            this.TP_VS_B_SELECT_SOUND.UseVisualStyleBackColor = true;
            this.TP_VS_B_SELECT_SOUND.Click += new System.EventHandler(this.TP_VS_B_SELECT_SOUND_Click);
            // 
            // TP_VS_B_SELECT_VIDEO_OUT
            // 
            this.TP_VS_B_SELECT_VIDEO_OUT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TP_VS_B_SELECT_VIDEO_OUT.Location = new System.Drawing.Point(416, 176);
            this.TP_VS_B_SELECT_VIDEO_OUT.Name = "TP_VS_B_SELECT_VIDEO_OUT";
            this.TP_VS_B_SELECT_VIDEO_OUT.Size = new System.Drawing.Size(75, 23);
            this.TP_VS_B_SELECT_VIDEO_OUT.TabIndex = 6;
            this.TP_VS_B_SELECT_VIDEO_OUT.Text = "Select";
            this.TP_VS_B_SELECT_VIDEO_OUT.UseVisualStyleBackColor = true;
            this.TP_VS_B_SELECT_VIDEO_OUT.Visible = false;
            // 
            // TP_VS_L_VIDEO
            // 
            this.TP_VS_L_VIDEO.AutoSize = true;
            this.TP_VS_L_VIDEO.Location = new System.Drawing.Point(7, 36);
            this.TP_VS_L_VIDEO.Name = "TP_VS_L_VIDEO";
            this.TP_VS_L_VIDEO.Size = new System.Drawing.Size(121, 13);
            this.TP_VS_L_VIDEO.TabIndex = 7;
            this.TP_VS_L_VIDEO.Text = "Video In (can drag drop)";
            // 
            // TP_VS_L_SOUND
            // 
            this.TP_VS_L_SOUND.AutoSize = true;
            this.TP_VS_L_SOUND.Location = new System.Drawing.Point(7, 97);
            this.TP_VS_L_SOUND.Name = "TP_VS_L_SOUND";
            this.TP_VS_L_SOUND.Size = new System.Drawing.Size(121, 13);
            this.TP_VS_L_SOUND.TabIndex = 8;
            this.TP_VS_L_SOUND.Text = "SOUND (can drag drop)";
            // 
            // TP_VS_L_VIDEO_OUT
            // 
            this.TP_VS_L_VIDEO_OUT.AutoSize = true;
            this.TP_VS_L_VIDEO_OUT.Location = new System.Drawing.Point(7, 162);
            this.TP_VS_L_VIDEO_OUT.Name = "TP_VS_L_VIDEO_OUT";
            this.TP_VS_L_VIDEO_OUT.Size = new System.Drawing.Size(144, 13);
            this.TP_VS_L_VIDEO_OUT.TabIndex = 9;
            this.TP_VS_L_VIDEO_OUT.Text = "Video Output (can drag drop)";
            // 
            // B_SITE
            // 
            this.B_SITE.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.B_SITE.Location = new System.Drawing.Point(6, 69);
            this.B_SITE.Name = "B_SITE";
            this.B_SITE.Size = new System.Drawing.Size(484, 64);
            this.B_SITE.TabIndex = 0;
            this.B_SITE.Text = "SITE";
            this.B_SITE.UseVisualStyleBackColor = true;
            this.B_SITE.Click += new System.EventHandler(this.B_SITE_Click);
            // 
            // B_SITE_UA
            // 
            this.B_SITE_UA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.B_SITE_UA.Location = new System.Drawing.Point(6, 139);
            this.B_SITE_UA.Name = "B_SITE_UA";
            this.B_SITE_UA.Size = new System.Drawing.Size(484, 59);
            this.B_SITE_UA.TabIndex = 1;
            this.B_SITE_UA.Text = "SITE UA";
            this.B_SITE_UA.UseVisualStyleBackColor = true;
            this.B_SITE_UA.Click += new System.EventHandler(this.B_SITE_UA_Click);
            // 
            // B_TG_BLOG
            // 
            this.B_TG_BLOG.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.B_TG_BLOG.Location = new System.Drawing.Point(6, 204);
            this.B_TG_BLOG.Name = "B_TG_BLOG";
            this.B_TG_BLOG.Size = new System.Drawing.Size(484, 66);
            this.B_TG_BLOG.TabIndex = 2;
            this.B_TG_BLOG.Text = "Telegram Blog";
            this.B_TG_BLOG.UseVisualStyleBackColor = true;
            this.B_TG_BLOG.Click += new System.EventHandler(this.B_TG_BLOG_Click);
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(518, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(534, 489);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.TP_VS.ResumeLayout(false);
            this.TP_VS.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TP_VS;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button TP_VS_B_RUN;
        private System.Windows.Forms.TextBox TP_VS_TB_VIDEO_OUT;
        private System.Windows.Forms.TextBox TP_VS_TB_SOUND;
        private System.Windows.Forms.TextBox TP_VS_TB_VIDEO_IN;
        private System.Windows.Forms.Label TP_VS_L_VIDEO_OUT;
        private System.Windows.Forms.Label TP_VS_L_SOUND;
        private System.Windows.Forms.Label TP_VS_L_VIDEO;
        private System.Windows.Forms.Button TP_VS_B_SELECT_VIDEO_OUT;
        private System.Windows.Forms.Button TP_VS_B_SELECT_SOUND;
        private System.Windows.Forms.Button TP_VS_B_SELECT_VIDEO_IN;
        private System.Windows.Forms.Button B_TG_BLOG;
        private System.Windows.Forms.Button B_SITE_UA;
        private System.Windows.Forms.Button B_SITE;
    }
}

