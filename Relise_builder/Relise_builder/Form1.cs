﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Relise_builder
{
    public partial class Form1 : Form
    {
        String ffmpeg = "";
        String prefix = "";
        String CurrentVersion = "0.1";

        public Form1()
        {
            InitializeComponent();
            string currentPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            ffmpeg = currentPath+"\\Module\\FFMPEG\\ffmpeg.exe";
            //MessageBox.Show(ffmpeg);

            this.Text = $@"VOLCUA Release Builder [{CurrentVersion}]";
            this.CenterToScreen();

            if (!File.Exists(currentPath+"\\Activ.lic")) 
            {
                Function_no_activated();
            }
        }

        void Function_no_activated() 
        {
            prefix = "[VOLCUA.COM.UA or COM]";

            this.Text = this.Text + "[Trial]";

            TrialWindows TW = new TrialWindows();
            TW.ShowDialog();
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void TP_VS_B_RUN_Click(object sender, EventArgs e)
        {
            if ( 
                File.Exists( TP_VS_TB_VIDEO_IN.Text) &&
                File.Exists( TP_VS_TB_SOUND.Text)
                )
            {
                Function_VideoPlusSound(TP_VS_TB_VIDEO_IN.Text, TP_VS_TB_SOUND.Text, TP_VS_TB_VIDEO_OUT.Text);

                TP_VS_TB_VIDEO_IN.Text = TP_VS_TB_SOUND.Text = TP_VS_TB_VIDEO_OUT.Text = "";
            }
        }


        void Function_VideoPlusSound(string Video_in, string Sound, string Video_out) 
        {

            string ffmpegTempPath = Path.Combine(Path.GetTempPath(), ffmpeg);


            // Command to execute ffmpeg and mix the video and audio
            string ffmpegCommand = $@"-y -fflags +genpts -i ""{Video_in}"" -i ""{Sound}"" -c:v copy -c:a aac -strict experimental ""{Video_out}""";


            try
            {
                ProcessStartInfo processInfo = new ProcessStartInfo
                {
                    FileName = ffmpeg,
                    Arguments = ffmpegCommand,
                    //CreateNoWindow = false,
                    //UseShellExecute = false,
                    //RedirectStandardOutput = true,
                    //RedirectStandardError = true
                };

                using (Process process = new Process())
                {
                    process.StartInfo = processInfo;
                    process.ErrorDataReceived += (s, e) => Console.WriteLine(e.Data);
                    process.OutputDataReceived += (s, e) => Console.WriteLine(e.Data);

                    process.Start();
                    //process.BeginOutputReadLine();
                    //process.BeginErrorReadLine();

                    process.WaitForExit();
                }

                Console.WriteLine("FFmpeg command executed successfully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred while running FFmpeg: " + ex.Message);
            }
        }

        private void TP_VS_B_SELECT_VIDEO_IN_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                TP_VS_TB_VIDEO_IN.Text = openDialog.FileName;

                TP_VS_TB_VIDEO_OUT.Text = openDialog.FileName.Replace(TP_VS_TB_VIDEO_OUT.Text.Split('\\')[TP_VS_TB_VIDEO_OUT.Text.Split('\\').Length-1],prefix+"[HS]"+ TP_VS_TB_VIDEO_OUT.Text.Split('\\')[TP_VS_TB_VIDEO_OUT.Text.Split('\\').Length - 1]);
            }
        }

        private void TP_VS_B_SELECT_SOUND_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Title = "Select A File";

            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                TP_VS_TB_SOUND.Text = openDialog.FileName;
            }
        }

        private void TP_VS_TB_VIDEO_IN_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void TP_VS_TB_VIDEO_IN_DragDrop(object sender, DragEventArgs e)
        {
            string[] Files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            foreach (string CFile in Files)
            {
                TP_VS_TB_VIDEO_IN.Text = CFile;

                TP_VS_TB_VIDEO_OUT.Text = CFile.Replace(CFile.Split('\\')[CFile.Split('\\').Length - 1], prefix + "[HS]" + CFile.Split('\\')[CFile.Split('\\').Length - 1]);

            }
        }

        private void TP_VS_TB_SOUND_DragEnter(object sender, DragEventArgs e)
        {
            //MessageBox.Show("test");
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.All;
            else
                e.Effect = DragDropEffects.None;
        }

        private void TP_VS_TB_SOUND_DragDrop(object sender, DragEventArgs e)
        {
            string[] Files = (string[])e.Data.GetData(DataFormats.FileDrop, false);

            foreach (string CFile in Files) 
            {
                TP_VS_TB_SOUND.Text = CFile;
            }
        }

        private void B_TG_BLOG_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://t.me/volcua_blog");
        }

        private void B_SITE_UA_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://volcua.com.ua/release/soft/volcua-release-builder/");
        }

        private void B_SITE_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Site in current time in dev");
        }
    }
}
