﻿
namespace Relise_builder
{
    partial class TrialWindows
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrialWindows));
            this.label1 = new System.Windows.Forms.Label();
            this.B_CloseTrialWindows = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(94, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(301, 312);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // B_CloseTrialWindows
            // 
            this.B_CloseTrialWindows.Location = new System.Drawing.Point(161, 340);
            this.B_CloseTrialWindows.Name = "B_CloseTrialWindows";
            this.B_CloseTrialWindows.Size = new System.Drawing.Size(182, 43);
            this.B_CloseTrialWindows.TabIndex = 2;
            this.B_CloseTrialWindows.Text = "Close";
            this.B_CloseTrialWindows.UseVisualStyleBackColor = true;
            this.B_CloseTrialWindows.Click += new System.EventHandler(this.B_CloseTrialWindows_Click);
            // 
            // TrialWindows
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 395);
            this.ControlBox = false;
            this.Controls.Add(this.B_CloseTrialWindows);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(503, 434);
            this.MinimumSize = new System.Drawing.Size(503, 434);
            this.Name = "TrialWindows";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Trial";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button B_CloseTrialWindows;
        private System.Windows.Forms.Timer timer1;
    }
}